class atla extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    reorderHeader(content);
    addIDtoFig(content);
    componentClass(content);
    bibliography(content);
    removeSupCallout(content);
    removeP(content);
    reorderComponent(content);
    letterNum(content);
    cleanToc(content);
    cleanLink(content);
    cleanHead(content);
    cleanQuote(content);
    //   restartNumber(content);
    //   spanH3(content);
    cleanComponentStart(content);
    noHyphen(content);
    cleanSmallCaps(content);
    markBalance(content);
    dropCapUpdate(content);
    mergeFigures(content)
    // resetEndNotesNumbering(content)

    resetChapterNotesNumbering(content);
    findJapan(content);
  }

  afterParsed(parsed) {
    imageRatio(parsed);
  }

  renderNode(node, sourceNode) {
    // console.log(node);
    // console.log(node.tagName);

    if (node.previousElementSibling) {
      if (node.previousElementSibling.tagName === "FIGURE") {
        const el = node.element;
        startBaseline(node, 18);
      }
    }
  }

  afterPageLayout(pageElement, page, breakToken) {
    pageElement.querySelectorAll("figure").forEach(img => {
      if (img.nextElementSibling) {
        img.nextElementSibling.style.paddingTop = `0`;
      }
      imageToTop(img);
      startBaseline(img.nextElementSibling, 18);
    });
  }

  afterRendered(pages) {
 

    pages.forEach(page => {
      // this.removeHyphens(page.area);
    });

    balanceText();
    cleanListTextAlignLast();
  }

  removeHyphens(element) {
    // Get all text nodes
    let textNodes = document.createTreeWalker(
      element,
      NodeFilter.SHOW_TEXT,
      null,
      null
    );

    let textNode;
    while ((textNode = textNodes.nextNode())) {
      let rects = this.getClientRects(textNode);
      let text = textNode.nodeValue;
      // Find all $shy
      let shys = /\u00AD/g;
      let m;

      let toRemove = [];
      do {
        m = shys.exec(text);
        if (m) {
          // Create a range containing the
          let range = document.createRange();
          range.setStart(textNode, m.index);
          range.setEnd(textNode, m.index);

          // Get Shy position
          let shy = range.getBoundingClientRect();

          // Check all lines of the text node
          let rect;
          for (var i = 0; i != rects.length; i++) {
            rect = rects[i];
            // If shy is on the same line
            if (shy.top == rect.top && shy.bottom == rect.bottom) {
              // Check if shy pos is less then line end
              if (Math.floor(shy.right) < Math.floor(rect.right)) {
                // remove shy
                if (text.charCodeAt(m.index) === 173) {
                  toRemove.push(m.index);
                }
              } else {
                // leave shy as is
                break;
              }
            }
          }
        }
      } while (m);

      // Remove all unneeded shys
      let removed = 0;
      let length = text.length;
      for (var i = 0; i < toRemove.length; i++) {
        let index = toRemove[i] - removed;

        // This should always be 173, but if it is not we should not remove it.
        // This happens sometimes - not sure why
        if (text.charCodeAt(index) === 173) {
          // extract the shy
          let start = text.slice(0, index);
          let end = text.slice(index + 1);
          text = start + end;
          removed = length - text.length; // subtract removed char
        }
      }

      // set node text bakc
      textNode.nodeValue = text;
    }
  }

  getClientRects(element) {
    if (!element) {
      return;
    }
    let rect;
    if (typeof element.getClientRects !== "undefined") {
      rect = element.getClientRects();
    } else {
      let range = document.createRange();
      range.selectNode(element);
      rect = range.getClientRects();
    }
    return rect;
  }
}

//  remove unused shy and keep hyphens

Paged.registerHandlers(atla);

function componentClass(content) {
  // use title of the componnet as class for the section
  // If the section contain a h1.ct which contains [ ]
  // get the content of the h1.ct, clean it and pass it as a class for the section

  const componentTitles = content.querySelectorAll(".front-component .ct");
  componentTitles.forEach(title => {
    if (title.textContent.includes("[")) {
      const componentClass = title.textContent
        .toLowerCase()
        .normalize()
        .replace(/\s/, "-")
        .replace("[", "")
        .replace("]", "");
      // .replace("&shy;", "");
      title.closest("section").setAttribute("class", componentClass);

      const header = title.closest("header");
      header.style.display = "none";
      // header.parentNode.removeChild(header);
    }
  });
}

function bibliography(content) {
  // bibliography add 3emdash (would be better to use 3 em dashes)

  const bibliographies = content.querySelectorAll(".bibliography-entry");
  bibliographies.forEach(bib => {
    if (bib.textContent.includes("⸻")) {
      const span = document.createElement("span");
      span.classList.add("emdash");
      span.innerHTML = "———";
      bib.innerHTML = bib.innerHTML.replace(/⸻/g, "");
      bib.insertAdjacentElement("afterbegin", span);
    }
    if (bib.textContent.includes("———")) {
      const span = document.createElement("span");
      span.classList.add("emdash");
      span.innerHTML = "———";
      bib.innerHTML = bib.innerHTML.replace(/———/g, "");
      bib.insertAdjacentElement("afterbegin", span);
    }
  });
}

function removeSupCallout(content) {
  const calloutsSup = content.querySelectorAll(".inline-note-callout sup");
  calloutsSup.forEach(callout => {
    let text = callout.textContent;
    callout.closest(".inline-note-callout").innerHTML = text;
  });
}

function resetEndNotesNumbering(content) {
  // remove sup element from the callout
  const component = content.querySelectorAll(
    '[class*="front"],[class*="body-"], [class*="back"]'
  );
  let j = 0;
  component.forEach(comp => {
    j = 0;
    const callouts = comp.querySelectorAll(".inline-note-callout");
    callouts.forEach(function(call, index) {
      call.setAttribute("href", `#ch${j}-en${index + 1}`);
      call.innerHTML = `${index + 1}`;
    });
    j++;
  });
}

function noHyphen(content) {
  content.querySelectorAll(`[class^="toc-"]`).forEach(toc => {
    toc.classList.add("donthyphenate");
  });
}

function removeP(content) {
  // remove empty ps if at the end of the component
  // replace empty p with p &nbsp; to keep empty lines

  const ps = content.querySelectorAll("p");
  if (ps.length != 0) {
    ps.forEach(el => {
      if (el.textContent == "") {
        el.textContent = "\u00A0";
      }
    });

    // remove p at the end of the component
    for (let i = ps.length - 1; i != 0; i--) {
      if (ps[i].textContent == "\u00A0" && !ps[i].nextElementSibling) {
        ps[i].parentNode.removeChild(ps[i]);
      }
    }
  }
}

function letterNum(content) {
  // part numbering in letter for the table of content: set the word as an array:
  var numberPart = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
    "eleven",
    "twelve",
    "thirteen",
    "fourteen",
    "fifteen",
    "sixteen",
    "seventeen",
    "eighteen",
    "nineteen",
    "twenty"
  ];

  // loop all the li of the nav and renumber at each part
  let i, j;
  i = 0;
  content.querySelectorAll("section.body-part").forEach((item, index) => {
    if (item.classList.contains("body-part")) {
      item.style.setProperty("--counter-part", '"Part ' + numberPart[i] + '"');
      i++;
    }
  });
}

function reorderComponent(content) {
  // send the table of content to its right location if there is a component with [contents]
  const previousToc = content.querySelector("section.toc");
  const newToc = content.querySelector("section.contents");

  if (previousToc && newToc) {
    newToc.insertAdjacentElement("afterend", previousToc);
    newToc.parentNode.removeChild(newToc);
  }
  // send the notes part to its right location if there is a notes before the end notes
  // find noteselement
  const backTitles = content.querySelectorAll(".back-component .ct");
  backTitles.forEach(title => {
    if (
      title.textContent.includes("Notes") &&
      title.closest("section").id != "comp-notes-0"
    ) {
      title.closest("section").setAttribute("id", "notes");
    }
  });

  const previousNotes = content.querySelector("section#comp-notes-0");
  const newNotes = content.querySelector("section#notes");

  if (previousNotes && newNotes) {
    previousNotes.setAttribute("id", "comp-notes-0");
    newNotes.insertAdjacentElement("afterend", previousNotes);
    newNotes.parentNode.removeChild(newNotes);
  }

  // remove editoria-built title-page

  const titlepage = content.querySelector("section.titlepage");
  if (titlepage) {
    titlepage.parentNode.removeChild(titlepage);
  }
}

function cleanToc(content) {
  // move notes link in the TOC
  // hide element that include [] from the table of content
  const tocList = content.querySelectorAll('li[class*="toc"]');
  tocList.forEach(tocLi => {
    if (tocLi.textContent.includes("[")) {
      tocLi.style.display = "none";
    }
  });

  tocList.forEach(tocLi => {
    if (tocLi.querySelector("a").getAttribute("href") === "#comp-notes-0") {
      tocLi.parentNode.removeChild(tocLi);
    }
    if (tocLi.textContent.includes("Notes")) {
      tocLi.querySelector("a").setAttribute("href", "#comp-notes-0");
    }
  });
}

function cleanLink(content) {
  //add wbr to / in links
  const links = content.querySelectorAll('a[href^="http"], a[href^="www"]');
  links.forEach(link => {
    // Rerun to avoid large spaces. Break after a colon or a double slash (//) or before a single slash (/), a tilde (~), a period, a comma, a hyphen, an underline (_), a question mark, a number sign, or a percent symbol.
    const content = link.textContent;
    let printableUrl = content.replace(/\/\//g, "//\u003Cwbr\u003E");
    printableUrl = printableUrl.replace(/\,/g, ",\u003Cwbr\u003E");
    // put wbr around everything.
    printableUrl = printableUrl.replace(
      /(\/|\~|\-|\.|\,|\_|\?|\#|\%)/g,
      "\u003Cwbr\u003E$1"
    );
    // turn hyphen in non breaking hyphen
    printableUrl = printableUrl.replace(/\-/g, "\u003Cwbr\u003E&#x2011;");
    link.setAttribute("data-print-url", printableUrl);
    link.innerHTML = printableUrl;
  });
}

function cleanHead(content) {
  // add unbreakable space after And, the, one, a, an, etc. in the headers.
  const headers = content.querySelectorAll(
    "h1, h2, h3, h4, .title-page p.start"
  );
  headers.forEach(header => {
    if (!(header instanceof Node) || header.nodeType !== Node.ELEMENT_NODE)
      return;
    var children = header.childNodes,
      node;
    for (var i = 0; children[i]; ++i) {
      node = children[i];
      switch (node.nodeType) {
        case Node.TEXT_NODE:
          // add a nbsp after articles and adverb for headers.
          node.nodeValue = node.nodeValue.replace(
            /(?<=\b(the|a|an|on|and|in)) /gi,
            "\u00A0"
          );
          break;
      }
    }
  });
}

function cleanQuote(content) {
  const blockquotes = content.querySelectorAll("blockquote");

  blockquotes.forEach(bq => {
    const wrap = document.createElement("P");
    wrap.innerHTML = bq.innerHTML;
    bq.innerHTML = "";
    bq.appendChild(wrap);
  });

  blockquotes.forEach(bq => {
    if (
      bq.previousElementSibling &&
      bq.previousElementSibling.tagName === "BLOCKQUOTE"
    ) {
      bq.innerHTML = bq.previousElementSibling.innerHTML + bq.innerHTML;
      bq.previousElementSibling.parentNode.removeChild(
        bq.previousElementSibling
      );
    }
  });

  // put cite in previous blockquotes
  const cites = content.querySelectorAll("blockquote + cite");

  cites.forEach(cite => {
    const quote = cite.previousElementSibling;
    quote.appendChild(cite);
  });
}

function restartNumber(content) {
  const restartPoint = content.querySelector(
    '.front-component + [class*="body"]'
  );
  if (restartPoint) {
    const restart = document.createElement("span");
    restart.classList.add("restart");
    restartPoint.insertAdjacentElement("afterbegin", restart);
  }
}

function spanH3(content) {
  const titles3 = content.querySelectorAll("h3");
  titles3.forEach(titleBis => {
    const spanh3 = document.createElement("span");
    spanh3.classList.add("h3");
    spanh3.innerHTML = titleBis.innerHTML;
    titleBis.nextElementSibling.classList.add("heading3");
    titleBis.nextElementSibling.insertAdjacentElement("afterbegin", spanh3);
    titleBis.parentNode.removeChild(titleBis);
  });
}

function cleanComponentStart(content) {
  // move any author to the header of the chapter

  const authors = content.querySelectorAll("header + p.author");
  authors.forEach(author => {
    author.previousElementSibling.insertAdjacentElement("beforeend", author);
  });

  // Retrieves an array of blockquote directly following a header
  let chainStarts = content.querySelectorAll("header + blockquote");

  let results = [...chainStarts].map(node => {
    return parseChain([], node);
  });
  results.forEach(chain => {
    let header = chain[0].previousElementSibling;
    chain.forEach(blockquote => {
      header.insertAdjacentElement("beforeend", blockquote);
    });
  });

  //  add a class `start` to the first element after a header
  const firsts = content.querySelectorAll("header + *");
  firsts.forEach(first => {
    first.classList.add("start");
  });
}

function startBaseline(element, baseline) {
  // snap element after specific element on the baseline grid.
  baseline = 18;

  if (element) {
    const elementOffset = element.offsetTop;

    const elementline = Math.floor(elementOffset / baseline);

    if (elementline != baseline) {
      const nextPline = (elementline + 1) * baseline;

      if (!(nextPline - elementOffset == baseline)) {
        element.style.paddingTop = `${nextPline - elementOffset}px`;
      }
    }
  }
}

// Returns an array of blockquotes that directly follow the provided blockquote element, including this provided blockquote
function parseChain(targetedQuotes, node) {
  let nextSibling = node.nextElementSibling;
  // If the next sibling is a blockquote...
  if (nextSibling && nextSibling.nodeName == "BLOCKQUOTE") {
    // Continue parsing the chain while adding the current node to the resulting array
    return parseChain([...targetedQuotes, node], nextSibling);
  } else {
    // If not, simply add the current node to the resulting array and return the result
    return [...targetedQuotes, node];
  }
}

function imageRatio(parsed) {
  let imagePromises = [];
  let images = parsed.querySelectorAll("img");
  images.forEach(image => {
    let img = new Image();
    let resolve, reject;
    let imageLoaded = new Promise(function(r, x) {
      resolve = r;
      reject = x;
    });

    img.onload = function() {
      let height = img.naturalHeight;

      let width = img.naturalWidth;

      let ratio = width / height;
      if (ratio >= 1.4) {
        image.classList.add("landscape");
        image.parentNode.classList.add("fig-landscape");
      } else if (ratio <= 0.8) {
        image.classList.add("portrait");
        image.parentNode.classList.add("fig-portrait");
      } else if (ratio < 1.39 || ratio > 0.8) {
        image.classList.add("square");
        image.parentNode.classList.add("fig-square");
      }

      // all images as square
      // image.classList.add("square");
      // image.parentNode.classList.add("fig-square");

      resolve();
    };
    img.onerror = function() {
      reject();
    };

    img.src = image.src;

    imagePromises.push(imageLoaded);
  });
  return Promise.all(imagePromises).catch(err => {
    console.warn(err);
  });
}

// function move image to top after rendering
// temp1.parentElement.insertAdjacentElement('afterbegin',temp1);

function imageToTop(image) {
  const firstKid = image.parentElement.firstChild;
  // console.log(firstKid);
  if (firstKid.nodeName === "figure") {
    firstKid.insertAdjacentElement("afterend", image);
  } else {
    image.parentElement.insertAdjacentElement("afterbegin", image);
    image.style.marginBottom = `var(--font-lineHeight)`;
  }
}

function reorderHeader(content) {
  const headers = content.querySelectorAll(
    `[class^="body"] h1, [class^="body"] h2 , [class^="body"] h3 , [class^="body"] h4 , [class^="body"] h5, [class^="front"] h1, [class^="front"] h2 , [class^="front"] h3 , [class^="front"] h4 , [class^="front"] h5`
  );
  headers.forEach(header => {
    let tag;
    switch (header.tagName) {
      case "H5":
        tag = "H6";
        break;
      case "H4":
        tag = "H5";
        break;
      case "H3":
        tag = "H4";
        break;
      case "H2":
        tag = "H3";
        break;
      case "H1":
        if (!header.classList.contains("ct")) {
          tag = "H2";
        } else {
          tag = "none";
        }
        break;
    }
    if (tag != "none") {
      const el = document.createElement(tag);
      el.innerHTML = header.innerHTML;
      el.className = header.className
      header.parentElement.replaceChild(el, header);
      el.className = header.className
    }
  });
}

function cleanSmallCaps(content) {
  content.querySelectorAll("small-caps").forEach(el => {
    var d = document.createElement("span");
    d.classList.add("smallcaps");
    d.innerHTML = el.innerHTML;
    el.parentNode.replaceChild(d, el);
  });
}

function markBalance(content) {
  content.querySelectorAll("h1, h2, h3, h4, h5, h6").forEach(el => {
    el.classList.add("balance-text");
  });
}

function resetEndNotesNumbering(content) {
  // remove sup element from the callout
  const component = content.querySelectorAll(
    '[class*="front"],[class*="body-"], [class*="back"]'
  );

  let j = 0;
  component.forEach(comp => {
    j = 0;
    const callouts = comp.querySelectorAll(".inline-note-callout");
    callouts.forEach(function(call, index) {
      call.setAttribute("href", `#ch${j}-en${index + 1}`);
      call.innerHTML = `${index + 1}`;
      call.id = `callOut-note-ch${sectionIndex}-n${calloutIndex}`
    });

    j++;
  });
}
function resetChapterNotesNumbering(content) {
  const sections = content.querySelectorAll("section");
  sections.forEach((section, sectionIndex) => {
    const noteCallouts = section.querySelectorAll(".inline-note-callout");
    noteCallouts.forEach((noteCallout, calloutIndex) => {
      noteCallout.setAttribute(
        "href",
        `#note-ch${sectionIndex}-n${calloutIndex}`
      );
      noteCallout.id = `callOut-note-ch${sectionIndex}-n${calloutIndex}`
    });
    const notes = section.querySelectorAll(".notes li");
    notes.forEach((note, noteIndex) => {
      note.id = `note-ch${sectionIndex}-n${noteIndex}`;
      const noteBack = document.createElement("a");
      noteBack.classList.add("backLink");
      noteBack.innerHTML = "*";
      noteBack.setAttribute("href", `#callOut-note-ch${sectionIndex}-n${noteIndex}`);
      note.insertAdjacentElement("beforeend", noteBack);
    });
  });
}

function cleanListTextAlignLast() {
  const splitParent = document.querySelectorAll("[data-split-original]");
  splitParent.forEach(el => {
    if ((el.tagName = "ul")) {
      // el.style.textAlignLast = "unset";
    }
  });
}

function dropCapUpdate(content) {
  const dropcaps = content.querySelectorAll(".start");
  dropcaps.forEach(drop => {
    const letter = drop.innerText.charAt(0);
    const regx = /G|P|Q|Y|J/i;
    if (letter.match(regx)) {
      drop.classList.add("double-story");
    }
  });
}

function addIDtoFig(content) {
  const figures = content.querySelectorAll("figure");
  figures.forEach((fig, index) => {
    fig.id = `fig-${index}`;
    fig.childNodes.forEach(nd => {
      if (nd.nodeName === "TABLE") {
        fig.classList.add("table");
      }
    })
  });

}


function mergeFigures(content) {
  const figures = content.querySelectorAll("figure");

  figures.forEach(fig => {
    if (
      fig.previousElementSibling &&
      fig.previousElementSibling.tagName === "FIGURE"
    ) {
      fig.innerHTML = fig.previousElementSibling.innerHTML + fig.innerHTML;
      fig.previousElementSibling.parentNode.removeChild(
        fig.previousElementSibling
      );
    }
  });
}



function findJapan(content) {
  content.querySelectorAll(`[lang="ja"]`).forEach(jap => {
    jap.parentElement.classList.add("jap-in");
  } )
}


class RepeatingTableHeaders extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterPageLayout(pageElement, page, breakToken, chunker) {
    // Find all split table elements
    let tables = pageElement.querySelectorAll("table[data-split-from]");

    tables.forEach((table) => {
      // Get the reference UUID of the node
      let ref = table.dataset.ref;
      if (ref === table.dataset["split-from"]) {
          // skip itself to avoid duplicate headers
          return;
      }
      // Find the node in the original source
      let sourceTable = chunker.source.querySelector("[data-ref='"+ ref +"']");
      // Find if there is a header
      let header = sourceTable.querySelector("thead");
      if (header) {
        // Clone the header element
        let clonedHeader = header.cloneNode(true);
        // Insert the header at the start of the split table
        table.insertBefore(clonedHeader, table.firstChild);
      }
    });

  }
}

Paged.registerHandlers(RepeatingTableHeaders);

